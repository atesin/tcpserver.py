
"""
tcpserver.py version 2, by atesin nov2021, wtfpl license

python library to ease the creation of tcp servers
(like "nc -lc {script}"), with client event callback functions

get info and send issues at https://gitlab.com/atesin/tcpserver-py
"""


if __name__ != 'tcpserver':
  print("can't run this module directly")
  exit()
  
  
import socket, threading, time


# debug on/off : replace all {#"""*"""and print} adding or removing a leading #


class TcpServer():
    
  def __init__(self, listenAddr, listenPort, maxClients, onConnect, onAvailable, onClose):  # constructor
    #"""*"""and print('> TcpServer __init__', listenAddr, listenPort, maxClients)
    self.clients = []
    self.__server = socket.socket()
    self.__server.bind((listenAddr, listenPort))  # don't catch here, catch outside
    self.__server.listen()
    threading.Thread(target=self.__run, args=(maxClients, onConnect, onAvailable, onClose), daemon=True).start()
    

  def __enter__(self):
    #"""*"""and print('> __enter__')
    return self


  def __exit__(self, xType, xValue, xTraceback):
    #"""*"""and print('> __exit__')
    self.close()
  
  
  def __del__(self):  # not a destructor but a garbage cleaner
    #"""*"""and print('> __del__')
    self.close()
  

  def close(self):
    #"""*"""and print('> TcpServer stop')
    self.__server.close()
    self.__server = None
    
    
  def __run(self, maxClients, onConnect, onAvailable, onClose):
    #"""*"""and print('> __run')
    try:
      while True:
        client, srcAddr = self.__server.accept()
        if len(self.clients) < maxClients:
          self.clients.append(TcpClient(self, client, onConnect, onAvailable, onClose))
        else:
          client.close()
    except (OSError, AttributeError):  # accept in a closed or no socket
      pass
    for client in self.clients.copy():
      client.close()
    #"""*"""and print('> exit __run')
    
  
class TcpClient():
    
  def __init__(self, server, client, onConnect, onAvailable, onClose):
    #"""*"""and print('> TcpClient __init__', client)
    self.__server = server
    self.__client = client
    self.__buffer = bytearray()
    self.__lock = threading.Lock()
    self.__onClose = onClose
    onConnect(self)
    threading.Thread(target=self.__recvLoop, daemon=True).start()
    threading.Thread(target=self.__readLoop, args=(onAvailable,), daemon=True).start()
    
    
  def __recvLoop(self):
    #"""*"""and print('> __recvLoop')
    try:
      while True:
        data = self.__client.recv(4096)
        if not data:  # return instant 0 bytes: peer closed
          break
        self.__lock.acquire()
        self.__buffer += data
        self.__lock.release()
    except (ConnectionError, AttributeError):  # .recv() in a closed or no socket
      pass
    self.disconnect()
    #"""*"""and print('> exit __recvLoop')
  

  def disconnect(self):
    #"""*"""and print('> disconnect')
    if self.__client and self.address():
      self.__onClose(self)
      self.__client.close()
      self.__client = None
    
    
  def address(self):
    #"""*"""and print('> address')
    try:
      return self.__client.getpeername()
    except (OSError, AttributeError):  # .getpeername() in a closed or no socket
      return None
      

  def available(self):
    #"""*"""and print('> available')
    return len(self.__buffer)


  def readBytes(self, size=0, timeout=60):
    #"""*"""and print('> readBytes', size, timeout)
    if not size:
      size = self.available()
    data = bytearray()
    left = size
    start = time.time()
    while True:
      data += self.__buffer[:left]
      self.__lock.acquire()
      del self.__buffer[:left]
      self.__lock.release()
      if (left := size - len(data)) < 1 or time.time() - start >= timeout:
        return data
      time.sleep(0.005)  # relax a little so other threads could also do its tasks
    

  def readUntil(self, delim=b'\n', timeout=60):
    #"""*"""and print('> readBytes', delim, timeout)
    until = 0
    try:
      until = self.__buffer.index(delim) + len(delim)
    except ValueError:
      pass
    return self.readBytes(until, timeout)


  def send(self, data):
    #"""*"""and print('> send')
    try:
      self.__client.send(data)
      return True
    except (OSError, AttributeError):
      pass
    return False
    
    
  def close(self):
    #"""*"""and print('> TcpClient stop')
    self.disconnect()
    self.__lock.acquire()
    self.__buffer.clear()
    self.__lock.release()
    

  def __readLoop(self, onAvailable):
    #"""*"""and print('> __readLoop')
    try:
      while True:  
        if self.__buffer:
          onAvailable(self)
        if not (self.__buffer or self.__client):  # no more data nor client
          break
    except FloatingPointError:
      pass
    self.__server.clients.remove(self)  # forget this client from server
    self.__onClose(self)
    #"""*"""and print('> exit __readLoop')
 
 
