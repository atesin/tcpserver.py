# TcpServer.py

python library to ease the creation of tcp servers (like "nc -lc {script}").
multiclient, multithreaded, with client event callback functions

TcpServer.py is licensed under WTFPL and published without any warranty, i wrote it
just to learn python in a fun way so it may contain errors, you are adviced so
you use under your own responsibility without the right to complain or demand
anything, however kind and well meaning suggestions are appreciated

## usage

this class supports classic "close" and context manager "with" styles<br>
place tcpserver.py file in the same directory as your script and edit like this,
this example will start a basic telnet echo server listening on loopback interface, see available methods below

```
import tcpserver

def onConnect(myClient):
  print(myClient.address(), 'connected')

def onAvailable(myClient):
  print(myClient.address(), myClient.available(), 'new/unread bytes')
  myClient.send(myClient.readBytes())

def onClose(myClient):
  print(myClient.address(), 'closed')

''' do this (classic style) '''
myServer = tcpserver.TcpServer('127.0.0.1', 23, 10, onConnect, onAvailable, onClose)
while someCondition():
  doSomething()
myServer.close()

''' or do this other (context manager style) '''
with tcpserver.TcpServer('127.0.0.1', 23, 10, onConnect, onAvailable, onClose) as myServer:
  while someCondition():
    doSomething()
```

## available classes (2)

### TcpServer

#### responsibilities
- listen incoming connections (i.e.: request to operating system tcp/ip stack the interface+port reservation and traffic passing)
- on new connections estabilished spawn a new TcpClient task and pass callback funcions to handle client independently
- maintain a list with active clients
- close all clients when server ordered to shutdown

##### constructor: TcpServer(listenAddr, listenPort, maxClients, onConnect, onAvailable, onClose)
- listenAddr (str), listenPort (int):<br>
  tell operating system the wish to listen local interface ip address/tcp port,
  local ip '' (empty string) means listen all local interfaces

- maxClients (int):<br>
  the max number of active clients at any time before start closing incoming connections, don't set this too high (about 10)... since this is a little server and not suited for production i saw the need to put this limit to avoid uncontrolled clients pool grow and resources depleting 
  
- onConnect, onAvailable, onClose:<br>
  callback function names to call when client events occur, the signature is `myFunc(TcpClient)`

- exceptions:<br>
  can raise the (uncaught) exceptions of [socket.bind()](https://docs.python.org/3/library/socket.html#socket.socket.bind) using [AF_INET](https://docs.python.org/3/library/socket.html#socket-families) (tcp/ip) family, amongst other related

##### .clients
- the list where server tracks active clients, avoid direct operation,
  it is mainly intended to do simple tasks like getting stats and send broadcasts,
  prefer available server/client methods instead, or if no choice be very cautious
  to not mess shared resources

##### .close()
- explicitly disconnects all active clients, flush all Rx buffers, forget once connected clients and stops the server

### TcpClient

#### responsibilities
- manage a local buffer with received data from remote peer
- exchange data with remote peer
- call passed-in callback functions after some client event occur, current events are: client connected, new/unread received data, client disconnected (client disconnect event could trigger before in some local situations)
- monitor the connection with remote peer
- when remote connection closes and Rx buffer depletes, auto-destroys removing itself from server's client list

#### events
- onConnect: triggered after initialization completed, but just before monitor tasks get run
- onAvailable: triggered when monitor loop finds data in Rx buffer, then data should be consumed explicitly with `.read*()` methods
- onClose: the last action the client does, triggered after self-deletes from server's clients list

##### constructor: not needed
- **not intended to be called explicitly by external script**
- could mess something, rely on TcpServer to manage new clients instead
- if you feel curious anyway the way TcpServer spawns a new client is `TcpClient(self, {callbackFunctions}), the "self" is a way TcpClient have to know its server and remove itself from their client's list later when it closes

##### .address()
- returns a tuple (remoteIp (str), remotePort (int)) of remote endpont client socket,
  or None if client is disconnected but there is still unread data

##### .available()
- returns the length (int) of buffer unread data received from client at the moment of the call.
  does not imply client [dis]connection, for that use .address() instead

##### .readBytes(size [=available], timeout [=60])
- consume data from client Rx buffer, up to {size} bytes length (int, all available if 0 or omitted)
  or {timeout} seconds has passed (int, 60 if omitted) whatever occurs first, and returns it as bytearray object.
  can raise ValueError exceptions, don't use negative values or bad things could happen

##### .readUntil(delim [=b'\\n'], timeout [=60])
- consume data from client Rx buffer, until {delim} (bytes like sequence) has found or {timeout} seconds has passed
  (int, 60 if omitted) whatever occurs first, and returns it as bytearray object including delimiter found.
  can raise ValueError exceptions, don't use negative values or bad things could happen

##### .send(data)
- send data (bytes like sequence) to client immediately, and returns the send status (bool) (usually related with connection status).
  can raise ValueError or related exceptions

##### .disconnect()
- explicitly disconnect the remote client, though may still have unread data in local Rx buffer after disconnection

##### .close()
- disconnect remote client and discard own Rx buffer, these conditions will be catched soon by monitor loop that will clean up things

